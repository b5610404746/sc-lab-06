package two;

public abstract class Vehicle {
	String brand;
	public Vehicle(String brand) {
		this.brand = brand;
	}
	public abstract String getColor(); 
	
	public String toString(){
		return this.brand;
	}
}

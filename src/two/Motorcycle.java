package two;

public class Motorcycle extends Vehicle {
	
	public Motorcycle(String brand) {
		super(brand);
	}
	@Override
	public String getColor(){
		return "Color:Red";
	}
	public int getWheels(int wheels){
		 return wheels;
	}
}
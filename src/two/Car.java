package two;

public class Car extends Vehicle {
	
	public Car(String brand) {
		super(brand);
	}
	@Override
	public String getColor(){
		return "Color:Blue";
	}
	
	public int getWheels(int forwheels,int bawheels){
		 return forwheels+bawheels;
	}
}

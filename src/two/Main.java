package two;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Car car = new Car("Toyota");
		Motorcycle motor = new Motorcycle("Honda");
		//Overloading method
		System.out.println(car.getWheels(2,2));
		System.out.println(motor.getWheels(2));
		//polymorphism method
		Use(car);
		Use(motor);
	}
		
	public static void Use(Vehicle ve){
		System.out.println(ve.toString());
		System.out.println(ve.getColor());
	}
		
		
		
}



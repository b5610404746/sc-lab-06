package one;

public abstract class Vehicle {
	String brand;
	public Vehicle(){
		this.brand = "Select brand";
	}
	
	public Vehicle(String brand) {
		this.brand = brand;
	}
	
	
	public String toString(){
		return this.brand;
	}
}

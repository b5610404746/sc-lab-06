package one;

public class Main {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Overloading constructor
		Car car = new Car();
		Car car2 = new Car("Toyota");
		Vehicle car3 = new Car();
		Vehicle car4 = new Car("Honda");
		
		System.out.println(car.toString());
		System.out.println(car2.toString());
		System.out.println(car3.toString());
		System.out.println(car4.toString());
	}

}
